import Figure from "./Figure";
import Point from "./Point";

export default class Rectangle extends Figure {
  start: Point;
  width: number;
  height: number;

  constructor(p: Point = new Point(0, 0), w = 10, h = 10) {
    super();
    this.start = p;
    this.width = w;
    this.height = h;
  }

  area() {
    return this.width * this.height;
  }

  draw(canvas: any) {
    canvas.fillStyle = this.color.getRGB();
    canvas.fillRect(this.start.x, this.start.y, this.width, this.height);
  }
}
