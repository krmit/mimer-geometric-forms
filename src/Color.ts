export default class Color {
  red: string;
  green: string;
  blue: string;

  constructor(red = "00", green = "00", blue = "00") {
    this.red = red;
    this.green = green;
    this.blue = blue;
  }

  getRGB() {
    return "#" + this.red + this.green + this.blue;
  }
}
