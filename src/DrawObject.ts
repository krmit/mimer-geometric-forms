export interface Drawable {
  draw: (canvas: any) => void;
}

export default class DrawObject implements Drawable {
  pointSize = 20;
  static drawObjects: Drawable[] = [];

  constructor() {
    DrawObject.drawObjects.push(this);
  }

  static add(drawObject: Drawable) {
    DrawObject.drawObjects.push(drawObject);
  }

  draw(canvas: any) {}

  static render(canvas: any) {
    for (const o of this.drawObjects) {
      o.draw(canvas);
    }
  }
}
