import Color from "./Color";
import DrawObject from "./DrawObject";
import Point from "./Point";
import Rectangle from "./Rectangle";

export function main(node: HTMLElement) {
  const canvas = node.getContext("2d");
  let p = new Point(1, 1);
  let p1 = new Point(5, 5);
  let p2 = new Point(70, 7);
  let c = new Color("FF", "00", "FF");
  let r = new Rectangle(p1, 20, 40);
  let r2 = new Rectangle(p2, 20, 40);
  r2.setColor(c);
  DrawObject.render(canvas);
}
