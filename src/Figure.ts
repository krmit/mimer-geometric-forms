import Color from "./Color";
import DrawObject from "./DrawObject";

export default abstract class Figure extends DrawObject {
  color: Color;

  constructor() {
    super();
    this.color = new Color();
  }

  setColor(color: Color) {
    this.color = color;
  }

  abstract area(): number;
}
