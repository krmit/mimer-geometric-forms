import DrawObject from "./DrawObject";

export default class Point extends DrawObject {
  x: number;
  y: number;
  hide = true;

  constructor(x = 0, y = 0) {
    super();
    this.x = x;
    this.y = y;
  }

  show() {
    this.hide = false;
  }

  draw(canvas: any) {
    if (!this.hide) {
      canvas.fillStyle = "#0000FF";
      canvas.fillRect(this.x, this.y, this.pointSize, this.pointSize);
    }
  }
}
